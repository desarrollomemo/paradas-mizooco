export const environment = {
  production: true,
  urlBase: {
    dev: './assets/mockdata',
    prod: 'http://10.3.16.58:1880',
    socket: 'ws://10.3.16.58:1880'
  },
  //SOLO MIENTRAS NO SE TENGA LA NUEVA BD
  // endpoints: {
  //   maquinas: '/dev/Maquinas',
  //   estadoRed: '/estado_red_plcs',
  //   paradas: '/dev/get_paradas/',
  //   historial: '/dev/traer_historicos',
  //   saveParada: '/dev/guardar_causa_tipo_2/',
  //   horaInicio: '/dev/get_horainicio/',
  //   socket: '/ws/act_maquina'
  // }
  // DESCOMENTAR CUANDO YA TENGAMOS LA NUEVA BD
  endpoints: {
    maquinas: '/prod/Maquinas',
    estadoRed: '/estado_red_plcs',
    paradas: '/prod/get_paradas/',
    historial: '/prod/traer_historicos',
    saveParada: '/prod/guardar_causa_tipo_2',
    horaInicio: '/prod/get_horainicio/',
    socket: '/ws/act_maquina',
    socket2: '/ws/save_type_stop'
  }
};
