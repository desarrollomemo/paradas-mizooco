export enum StateMachineTag {
  success      = 'SUCCESS',
  danger       = 'DANGER',
  warning      = 'WARNING',
  disconnected = 'DISCONNECTED'
}

export type StateMachineMapType = {
  [key in StateMachineTag]: {
    estado: string;
    icon: string;
    shadow: string;
    color: string;
    bgColor: string;
    grdColor: string;
    varColor: string;
    hexadecimal: string;
  }
}

export const StateMachineMap:StateMachineMapType = {
  SUCCESS: {
    estado: 'En línea',
    icon: 'fa-thumbs-o-up',
    shadow: 'shadow_success',
    color: 'cr-success',
    bgColor: 'bg-success',
    grdColor: 'grd-success',
    varColor: 'var(--bs-success)',
    hexadecimal: '#198754'
  },
  DANGER: {
    estado: 'Parada',
    icon: 'fa-exclamation-triangle icon-warning',
    shadow: 'shadow_danger',
    color: 'cr-danger',
    bgColor: 'bg-danger',
    grdColor: 'grd-danger',
    varColor: 'var(--bs-danger)',
    hexadecimal: '#dc3545'
  },
  WARNING: {
    estado: 'En espera',
    icon: 'fa-clock-o',
    shadow: 'shadow_warning',
    color: 'cr-warning',
    bgColor: 'bg-warning',
    grdColor: 'grd-warning',
    varColor: 'var(--bs-warning)',
    hexadecimal: '#ffc107'
  },
  DISCONNECTED: {
    estado: 'Desconectada',
    icon: 'fa-chain-broken',
    shadow: 'shadow_disconnected',
    color: 'cr-disconnected',
    bgColor: 'bg-disconnected',
    grdColor: 'grd-disconnected',
    varColor: 'var(--bs-gray-500)',
    hexadecimal: '#adb5bd'
  }
}
