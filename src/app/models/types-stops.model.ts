export enum TitlesTypesStopsTag {
  paquetadoras = 'PQ',
  ensacadoras  = 'D',
  exclusion    = 'LX',
}
export type TitlesTypesStopsMapType = {
  [key in TitlesTypesStopsTag]: {
    title1?: string;
    title2?: string;
    title3?: string;
    title4?: string;
    title5?: string;
    title6?: string;
  }
}

export const TitlesTypesStopsMap: TitlesTypesStopsMapType = {
  PQ: {
    title1: 'Impresora',
    title2: 'Empacadora',
    title3: 'Enfardadora',
    title4: 'Sorter',
    title5: 'Producción',
    title6: 'Servicios generales',
  },
  D: {
    title1: 'Impresora',
    title2: 'Empacadora',
    title3: 'Selladora',
    title4: 'Cosedora',
    title5: 'Producción',
    title6: 'Servicios generales',
  },
  LX: {
    title1: 'Acondicionador',
    title2: 'Cañon',
    title3: 'Secador',
    title4: 'Engrase',
    title5: 'Producción',
    title6: 'Servicios generales',
  }
}
