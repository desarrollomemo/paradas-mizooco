import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs';
import { UnsubscribeComponent } from 'src/app/common/unsubscribe.component';
import { BehaviorService } from 'src/app/services/behavior.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends UnsubscribeComponent implements OnInit {
  loading:boolean = true;

  constructor(
    private behaviorSrv: BehaviorService
  ){
    super();
  }

  ngOnInit(): void {
    this.behaviorSrv.isLoadingObservable.pipe(takeUntil(this.destroy$)).subscribe({
      next: res =>{ this.loading = res; },
      error: err => { throw err; }
    })
  }

}
