
export interface StopsFiltered {
  title: string,
  acronym: string,
  data: Array<TypeStops>
}

export interface TypeStops {
  Codigo_maquina:       string;
  Descripcion_maquina:  string;
  Codigo_tipo_activo:   string;
  Codigo_Activo:        string;
  Descripcion_Activo:   string;
  Codigo_parada:        string;
  Descripcion_parada:   string;
  Codigo_Categoria:     number;
  Decripcion_Categoria: string;
  CADOP:                string;
  Descripcion_CADOP:    string;
}
