export interface HistorialStops {
  id_parada:        string;
  id_maquina:       string;
  Horainicio:       string;
  Horafin:          string;
  ID:               ID;
  Alerta:           string;
  Hora_tipo_parada: string;
}

export interface ID {
  type: string;
  data: number[];
}
