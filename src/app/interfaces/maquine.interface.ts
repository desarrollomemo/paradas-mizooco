export interface Machine {
  Codigo_Maquina: string;
  Descrp_Maquina: string;
  Estado:         number;
  Codigo_Proceso: string;
  status?:        string;
}

export const idsMachines: Array<string> = ['PQ0','PQ1','PQ2','PQ3','PQ4','PQ5','PQ6','PQ7','PQ8', 'PQ10', 'D1', 'D2', 'D3', 'D4', 'LX1', 'LX2', 'LX3', 'LX4' ];
