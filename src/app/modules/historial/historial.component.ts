import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { takeUntil } from 'rxjs';
import { UnsubscribeComponent } from 'src/app/common/unsubscribe.component';
import { HistorialStops } from 'src/app/interfaces/historial-stops.interface';
import { BehaviorService } from 'src/app/services/behavior.service';
import { ParadasService } from 'src/app/services/paradas.service';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { historialColumns } from 'src/app/models/historial.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.scss']
})
export class HistorialComponent extends UnsubscribeComponent implements OnInit, AfterViewInit  {
  displayedColumns: string[] = historialColumns;
  dataSource!: MatTableDataSource<HistorialStops>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  dataHistorial: Array<HistorialStops> = [];

  constructor(
    private paradaSrv: ParadasService,
    private behaviorSrv: BehaviorService,
    private paginatorIntl: MatPaginatorIntl,
    private toastr: ToastrService,
    private router: Router,
    private location: Location
  ){
    super();
    this.paginatorIntl.itemsPerPageLabel = "Registros por página";
  }

  ngOnInit(): void {
    this.behaviorSrv.hiddenLoading(true);
    this.getHistorial();
  }

  ngAfterViewInit() {

  }


  getHistorial() {
    this.paradaSrv.getHistorialStops().pipe(takeUntil(this.destroy$)).subscribe({
      next: (resp:any) =>{
        this.dataHistorial = resp;
        this.dataSource = new MatTableDataSource(this.dataHistorial);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.behaviorSrv.hiddenLoading(false);
      },
      error: err =>{
        this.router.navigate(['/maquinas']);
        this.toastr.error('No se pudo cargar el historial.', 'Lo sentimos!');
        throw err;
      }
    });
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  formatHour(input:any) {
    const fecha = new Date(input);
    let hora = fecha.getUTCHours().toString().padStart(2, '0');
    let minutos = fecha.getMinutes().toString().padStart(2, '0');
    let segundos = fecha.getSeconds().toString().padStart(2, '0');
    return `${hora}:${minutos}:${segundos}`;
  }


  calculateDurationSg(fin:any, inicio:any) {
    const dateOu:any = new Date(fin);
    const dateIn:any = new Date(inicio);
    return (dateOu - dateIn) /1000;
  }

  backPages() {
    this.location.back();
  }


}
