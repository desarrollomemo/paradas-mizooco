import { Component, OnInit, inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable, forkJoin, takeUntil } from 'rxjs';
import { UnsubscribeComponent } from 'src/app/common/unsubscribe.component';
import { Machine, idsMachines } from 'src/app/interfaces/maquine.interface';
import { BehaviorService } from 'src/app/services/behavior.service';
import { ParadasService } from 'src/app/services/paradas.service';
import { trigger, animate, transition } from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { SocketService } from 'src/app/services/socket.service';
import { SnackBarComponent } from 'src/app/components/snack-bar/snack-bar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { DialogLoginComponent } from 'src/app/components/dialog-login/dialog-login.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-paradas',
  templateUrl: './paradas.component.html',
  styleUrls: ['./paradas.component.scss'],
  animations: [
    trigger('panelChange', [
      transition('open => closed', [
        animate('1s')
      ]),
    ])
  ]
})
export class ParadasComponent extends UnsubscribeComponent implements OnInit {
  readonly dialog = inject(MatDialog);
  snackBar = inject(MatSnackBar);
  ids:any;
  idMachine:any;
  access:any;
  rest:Array<string> = idsMachines;
  machines: Array<Machine> = [];
  machineOnly: Array<Machine> = [];
  paquetadoras:Array<Machine> = [];
  paquetadorasOnline:Array<Machine> = [];
  ensacadoras:Array<Machine> = [];
  ensacadorasOnline:Array<Machine> = [];
  extrusion:Array<Machine> = [];
  extrusionOnline:Array<Machine> = [];
  dataUser:any = '';
  viewPQ: boolean = false;
  viewEN: boolean = false;
  viewEX: boolean = false;

  fork$: Observable<any> = new Observable();

  constructor(
    private paradasSrv: ParadasService,
    private behaviorSrv: BehaviorService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private socketSrv: SocketService,
    private location: Location
  ) {
    super();
  }

  ngOnInit(): void {

    this.dataUser = JSON.parse(localStorage.getItem('loginUser')!);        
    if (!this.dataUser) this.validateAccess();
    
    
    this.idMachine = this.route.snapshot.paramMap.get('id');
    this.behaviorSrv.hiddenLoading(true);
    this.initInfoMachines();

    this.socketSrv.onConnect();
    this.socketSrv.listen();

    this.socketSrv.isSocketObservable.pipe(takeUntil(this.destroy$)).subscribe({
      next: resp =>{
        console.log(resp);
        if(this.router.url.includes('/paradas') && resp.opt){
          this.machines = [];
          this.machineOnly = [];
          this.initInfoMachines();
          this.snackBar.openFromComponent(SnackBarComponent, {
            data: {icon: 'fa-exclamation-triangle', msg: `Movimiento detectado de la maquina ${resp.data.id}`},
            duration: 3000,
          });
        }
      }
    });
  }



  initInfoMachines() {
    forkJoin([
      this.paradasSrv.getInfoMachines(),
      this.paradasSrv.getStateRedPLC()
    ]).pipe(takeUntil(this.destroy$))
    .subscribe({
      next: ([machines, stateRed]:[any,any]) =>{
        this.ids = stateRed.filter((r:any) => r.estado_red == "offline");
        //this.machines = machines;
        this.machineOnly = machines.filter( (mach:any) => mach.Codigo_Maquina === this.idMachine )
        this.filterMachines(this.machineOnly);
        this.behaviorSrv.hiddenLoading(false);
        },
      error: err => {
        this.toastr.error('En este momento no es posible mostrar la información.', 'Lo sentimos!')
        this.behaviorSrv.hiddenLoading(false);
        throw err;
      },
    });
  }



  private filterMachines = (data:Array<Machine>) =>{
    data.forEach((machine:any) => {
      if (machine.Estado === 1) {
        machine.status = 'SUCCESS';
      } else if(machine.Estado === 0) {
        machine.status = 'DANGER';
      } else {
        machine.status = 'WARNING';
      }
      this.ids.forEach((item:any) => {
        if(item.id_maquina === machine.Codigo_Maquina){
          machine.status = 'DISCONNECTED';
        }
      });
      if (this.rest.includes(machine.Codigo_Maquina)) this.machines.push(machine);
    });
    this.filterMachineByCode(this.machines);
  }

  private filterMachineByCode(machines:Machine[]) {
    this.paquetadoras = machines.filter( (mach:Machine) => mach.Codigo_Maquina.indexOf('PQ') >= 0 ).filter(mach => mach.status !== 'SUCCESS');
    // this.ensacadoras = machines.filter( (mach:Machine) => mach.Codigo_Maquina.indexOf('D') >= 0 ).filter(mach => mach.status !== 'SUCCESS');
    // this.extrusion = machines.filter( (mach:Machine) => mach.Codigo_Maquina.indexOf('LX') >= 0 ).filter(mach => mach.status !== 'SUCCESS');
    this.paquetadorasOnline = machines.filter( (mach:Machine) => mach.Codigo_Maquina.indexOf('PQ') >= 0 ).filter(mach => mach.status === 'SUCCESS');
    // this.ensacadorasOnline = machines.filter( (mach:Machine) => mach.Codigo_Maquina.indexOf('D') >= 0 ).filter(mach => mach.status === 'SUCCESS');
    // this.extrusionOnline = machines.filter( (mach:Machine) => mach.Codigo_Maquina.indexOf('LX') >= 0 ).filter(mach => mach.status === 'SUCCESS');
  }


  viewOnlineMachine(type:string) {
    switch (type) {
      case 'PQ':
        this.viewPQ = !this.viewPQ;
        break;
      case 'D':
        this.viewEN = !this.viewEN;
        break;
      case 'LX':
        this.viewEX = !this.viewEX;
        break;

    }
  }



  validateAccess() {
    const dialog$ = this.dialog.open(DialogLoginComponent, {
      enterAnimationDuration:'100ms',
      exitAnimationDuration:'100ms',
      disableClose: true,
      data: {  }
    });
    dialog$.componentInstance.onSubmitAccept.subscribe({
      next: (resp:any) => {
        this.dataUser = resp;
        
      },
      error: (err:any) =>{throw err}
    })
  }


  getOut() {
    localStorage.removeItem('loginUser');
    this.dataUser = '';
    this.behaviorSrv.thereUser(null)
    location.reload();
  }





}
