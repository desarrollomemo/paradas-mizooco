import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ParadasComponent } from './paradas/paradas.component';
import { HistorialComponent } from './historial/historial.component';
import { TiposParadasComponent } from './tipos-paradas/tipos-paradas.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'paradas/:id', component: ParadasComponent },
      { path: 'historial', component: HistorialComponent},
      { path: 'tipos-paradas', component: TiposParadasComponent},
      // { path: '', redirectTo: 'paradas/:id', pathMatch: 'full' },
      // { path: '**', redirectTo: 'paradas/:id', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
