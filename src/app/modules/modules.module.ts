import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '../components/components.module';
import { ModulesRoutingModule } from './modules-routing.module';
import { MaterialModule } from '../material.module';

import { HistorialComponent } from './historial/historial.component';
import { ParadasComponent } from './paradas/paradas.component';
import { TiposParadasComponent } from './tipos-paradas/tipos-paradas.component';


@NgModule({
  declarations: [
    ParadasComponent,
    HistorialComponent,
    TiposParadasComponent
  ],
  imports: [
    CommonModule,
    ModulesRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    MaterialModule
  ]
})
export class ModulesModule { }
