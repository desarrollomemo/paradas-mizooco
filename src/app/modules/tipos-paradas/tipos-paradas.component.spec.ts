import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposParadasComponent } from './tipos-paradas.component';

describe('TiposParadasComponent', () => {
  let component: TiposParadasComponent;
  let fixture: ComponentFixture<TiposParadasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TiposParadasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TiposParadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
