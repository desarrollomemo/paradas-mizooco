import { ChangeDetectionStrategy, Component, OnInit, inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, takeUntil } from 'rxjs';
import { UnsubscribeComponent } from 'src/app/common/unsubscribe.component';
import { DialogStopsComponent } from 'src/app/components/dialog-stops/dialog-stops.component';
import { SnackBarComponent } from 'src/app/components/snack-bar/snack-bar.component';
import { Machine } from 'src/app/interfaces/maquine.interface';
import { StopsFiltered, TypeStops } from 'src/app/interfaces/type-stops.interface';
import { StateMachineMap } from 'src/app/models/state-machine.model';
import { TitlesTypesStopsMap } from 'src/app/models/types-stops.model';
import { BehaviorService } from 'src/app/services/behavior.service';
import { GeneralsService } from 'src/app/services/generals.service';
import { ParadasService } from 'src/app/services/paradas.service';
import { SocketService } from 'src/app/services/socket.service';
import { Location } from '@angular/common';



@Component({
  selector: 'app-tipos-paradas',
  templateUrl: './tipos-paradas.component.html',
  styleUrls: ['./tipos-paradas.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TiposParadasComponent extends UnsubscribeComponent implements OnInit {
  snackBar = inject(MatSnackBar);
  readonly dialog = inject(MatDialog);
  stops: Array<TypeStops> = [];
  stateMachineMap:any = StateMachineMap;
  titlesTypesStops:any = TitlesTypesStopsMap;
  dataMachine!: Machine;
  dataStops$: Observable<any> = new Observable();
  paroCod:string = '';
  descrpParo:string = '';
  acronym:string = '';
  onlyStop:boolean = true;
  isLoading: boolean = false;
  stopsFiltered: Array<StopsFiltered> = [];
  times!:{regParada:string, horaInicio:string};

  constructor(
    private paradasSrv: ParadasService,
    private behaviorSrv: BehaviorService,
    private generalsSrv: GeneralsService,
    private toastr: ToastrService,
    private socketSrv: SocketService,
    private router: Router,
    private location: Location
  ){
    super();
  }

  ngOnInit(): void {
    this.behaviorSrv.hiddenLoading(true);
    this.dataMachine = JSON.parse(localStorage.getItem('Machine')!);
    console.log(this.dataMachine.Codigo_Maquina);

    this.getTypesStops();

    this.socketSrv.onConnectStops();
    this.socketSrv.listenStops();

    //Se dispara cuando se detecta el envio de parada desde otro dispositivo
    this.socketSrv.isSocketStopsObservable.pipe(takeUntil(this.destroy$)).subscribe({
      next: resp =>{
        if(this.router.url.includes('/tipos-paradas') && resp.optStops){
          if (resp.data.id_maquina == this.dataMachine.Codigo_Maquina) {
            console.log(resp);
            this.snackBar.openFromComponent(SnackBarComponent, {
              data: {icon: 'fa-exclamation-triangle', msg: `Se acaba de actualizar el Tipo de Paro para esta máquina ${resp.data.id_maquina}`},
              duration: 10000,
            });
            setTimeout(() => { this.location.back(); }, 3000);
          }

        }

      }
    });
  }



  getTypesStops(){
    this.dataStops$ = this.paradasSrv.getTypeStops(this.dataMachine.Codigo_Maquina);
    this.dataStops$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (resp:any) =>{
        console.log(resp);

        this.behaviorSrv.hiddenLoading(false);
        this.stops = resp;

        this.acronym = this.stops[0]?.Codigo_maquina;
        this.stopsFiltered = this.generalsSrv.filtersTypeStops(this.stops, this.acronym);
      },
      error: err => {
        this.router.navigate(['/maquinas']);
        this.toastr.error('No se pudo cargar los tipos de parada.', 'Lo sentimos!');
        throw err;
      }
    });
  }


  selectTypeStop(data:TypeStops, i:number){
    const container = document.querySelector(`#${data.Codigo_parada}`) as HTMLElement;
    if (this.onlyStop) {
      this.paroCod = `${data.Codigo_parada}`;
      this.descrpParo = data.Descripcion_parada;
      container.classList.toggle('active-stop');
      this.idicatorSelected(data, true);
      this.onlyStop = false;
      return
    }
    //esta condición quita de la selección el tipo de paro
    if(`${data.Codigo_parada}` === this.paroCod) {
      container.classList.toggle('active-stop');
      this.paroCod = '';
      this.descrpParo = '';
      this.onlyStop = true;
      this.isLoading = false;
      this.idicatorSelected(data, false);
    }
  }



  saveTypeStop() {
    const today = new Date();
    // hora_tipo_parada: `${today.toLocaleDateString()} ${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`    
    
    const dialog$ = this.dialog.open(DialogStopsComponent, {
        enterAnimationDuration:'100ms',
        exitAnimationDuration:'100ms',
        disableClose: true,
        data: { codParo:this.paroCod, descrpParo:this.descrpParo }
      });
    dialog$.componentInstance.onSubmitAccept.subscribe({
      next: (resp:boolean) =>{
        if(resp){
          let IdUsuario:any = localStorage.getItem('loginUser');
          const json =  {
            id_maquina: this.dataMachine.Codigo_Maquina,
            horainicio: this.times.horaInicio,
            Estado: 2,
            tipo_paro: this.paroCod,
            id_reg_parada: this.times.regParada,
            descrp_paro: this.descrpParo,
            IdUsuario
          }          

          this.paradasSrv.postStop(json).pipe(takeUntil(this.destroy$)).subscribe({
            next: resp =>{
              console.log(resp);
              this.toastr.success('Tipo de parada guardado con éxito!', 'Bien hecho!');
              setTimeout(() => {
                this.isLoading = false;
                dialog$.close();
                this.router.navigate(['/maquinas']);
              }, 2000);
            },
            error: err =>{
              dialog$.close();
              this.toastr.error('No se pudo guardar el tipo de parada.', 'Lo sentimos!');
              throw err;
            }
          });
        } else {
          return;
        }
      },
      error: (err:any) =>{throw err}
    })
  }


  listenTimes(event:any) { this.times = event; }


  private idicatorSelected = (item:TypeStops, opt:boolean) =>{
    const IDContent = document.querySelector(`#${item.Codigo_tipo_activo}`) as HTMLElement;
    (opt) ? IDContent.classList.add('point') : IDContent.classList.remove('point');
  }



  setBorderLefColor = (status:string) => `border-left: 15px solid ${this.stateMachineMap[status!].varColor}`;
  setBorderColor = (status:string) => `border: 1px solid ${this.stateMachineMap[status!].varColor}`;


  backPages() {
    this.location.back();
  }

}


