import { Component, inject, ChangeDetectionStrategy, Inject, EventEmitter, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { BehaviorService } from 'src/app/services/behavior.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-dialog-login',
  templateUrl: './dialog-login.component.html',
  styleUrls: ['./dialog-login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogLoginComponent implements OnInit {
  onSubmitAccept = new EventEmitter();
  readonly dialogRef = inject(MatDialogRef<DialogLoginComponent>);
  isLoading: boolean = false;
  numUser:any = '';
  dataUser:any = '';

  constructor(
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data:any,
    private loginSrv: LoginService,
    private behaviorSrv: BehaviorService,
  ){}

  ngOnInit(): void {

  }

  confirm() {
    if (!this.numUser) {
      return
    }
    
    this.loginSrv.validateLoginService(this.numUser).subscribe({
      next: (resp:any) =>{
        this.dataUser = resp.find( (item:any) => item.numUsuario === this.numUser );

        if(this.dataUser) {
          this.isLoading = true;
          localStorage.setItem('loginUser', JSON.stringify(this.dataUser));
          this.dialogRef.close(false);
          this.toastr.success('Acceso permitido', 'Bien hecho!')
          this.behaviorSrv.thereUser(this.dataUser)
          this.onSubmitAccept.emit(this.dataUser);
        } else {
          this.toastr.error('Dato incorrecto.', 'Lo sentimos!');
        }
        
      },
      error: err =>{
        this.toastr.error('En este momento no es posible ingresar a la App.', 'Lo sentimos!');
        throw err;
      }
    })
    
  }


  close(){
    this.dialogRef.close(false);
  }


}
