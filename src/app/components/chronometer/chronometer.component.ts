import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { takeUntil } from 'rxjs';
import { UnsubscribeComponent } from 'src/app/common/unsubscribe.component';
import { Machine } from 'src/app/interfaces/maquine.interface';
import { StateMachineMap } from 'src/app/models/state-machine.model';
import { BehaviorService } from 'src/app/services/behavior.service';
import { ParadasService } from 'src/app/services/paradas.service';

@Component({
  selector: 'app-chronometer',
  templateUrl: './chronometer.component.html',
  styleUrls: ['./chronometer.component.scss']
})
export class ChronometerComponent extends UnsubscribeComponent implements OnInit {

  @Input() data!: Machine;
  @Output() objTimes = new EventEmitter<{}>();
  chronometer: string = '00:00:00';
  interval:any;
  stateMachineMap:any = StateMachineMap;
  regParada!: string;
  horaInicio: string = '';

  constructor(
    private paradasSrv: ParadasService,
    private behaviorSrv: BehaviorService,
  ){
    super();
  }

  ngOnInit(): void {
    setTimeout(() => {      
      this.initChronometer();
    }, 500);
  }

  override ngOnDestroy(): void {
    this.stopInterval()
  }


  initChronometer() {
    this.paradasSrv.getHourInit(this.data.Codigo_Maquina).pipe(takeUntil(this.destroy$)).subscribe({
      next: (resp:any) =>{
        if(!resp.length) return;
        this.regParada = this.convertirId(resp[0].ID.data);
        this.horaInicio = resp[0].Horainicio;
        this.sendTimeStop();
        this.interval = setInterval(() => {
          //const tiempo = new Date().getTime() - new Date(this.horaInicio).getTime() - 1000 * 60 * 60 * 5;
          const tiempo = new Date().getTime() - new Date(this.horaInicio).getTime();
          this.chronometer = this.formatoTiempo(tiempo);
          
        }, 1000);
      },
      error: err =>{ throw err;}
    })
  }

  sendTimeStop(){
    this.objTimes.emit({regParada: this.regParada, horaInicio:this.horaInicio});
  }


  convertirId(bytes:any) {
    const hexString = Array.from(bytes).map( (byte:any) =>{
      return (`0` + byte.toString(16)).slice(-2).toUpperCase()
    }).join('');
    return `0x${hexString}`;
  }


  formatoTiempo(milisegundos:number) {
    let horas = 0;
    let minutos = 0;
    let segundos = 0;
    if (milisegundos > 1000) {
      const aux = milisegundos;
      milisegundos = milisegundos % 1000;
      segundos = (aux - milisegundos) / 1000;
      if (segundos > 60) {
        const aux2 = segundos;
        segundos = segundos % 60;
        minutos = (aux2 - segundos) / 60;
        if (minutos > 60) {
          const aux3 = minutos;
          minutos = minutos % 60;
          horas = (aux3 - minutos) / 60;
        }
      }
    }
    const h = horas < 10 ? "0" + horas : horas;
    const m = minutos < 10 ? "0" + minutos : minutos;
    const s = segundos < 10 ? "0" + segundos : segundos;
    milisegundos < 10
      ? "00" + milisegundos
      : milisegundos < 100
        ? "0" + milisegundos
        : milisegundos;
    return `${h}:${m}:${s}`;
  }

  stopInterval() {
    clearInterval(this.interval)
    this.chronometer = '00:00:00';
  }

}
