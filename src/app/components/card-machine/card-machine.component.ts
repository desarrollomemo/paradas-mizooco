import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs';
import { UnsubscribeComponent } from 'src/app/common/unsubscribe.component';
import { Machine } from 'src/app/interfaces/maquine.interface';
import { TypeStops } from 'src/app/interfaces/type-stops.interface';
import { StateMachineMap } from 'src/app/models/state-machine.model';
import { BehaviorService } from 'src/app/services/behavior.service';
import { ParadasService } from 'src/app/services/paradas.service';

@Component({
  selector: 'app-card-machine',
  templateUrl: './card-machine.component.html',
  styleUrls: ['./card-machine.component.scss']
})
export class CardMachineComponent extends UnsubscribeComponent implements OnInit {

  _data!:Machine;
  @Input()
  get dataMachine():Machine{ return this._data; }
  set dataMachine(data:Machine){ this._data = data; }

  @Input() dataUser: any;

  stateMachineMap:any = StateMachineMap;

  constructor(
    private router:Router,
    private behaviorSrv: BehaviorService,
  ){
    super();
  }

  ngOnInit(): void {
    this.behaviorSrv.isUserObservable.subscribe({
      next: (resp:any) =>{
        this.dataUser = resp
        console.log(this.dataUser);
      }
    }) 
  }

  textButton(data:string|any){
    let text = '';
    switch (data) {
      case 'SUCCESS':
        text = 'En producción';
        return text;
      case 'WARNING':
        text = 'Esperando...';
        return text;
      case 'DANGER':
        text = 'Ver paradas';
        return text;
      default:
        text = 'Ver paradas';
        return text;
    }
  }

  navigateTypeStop(machine:any) {
    if(machine.status === 'SUCCESS' || machine.status === 'WARNING') return;
    localStorage.removeItem("Machine");
	  localStorage.setItem("Machine", JSON.stringify(machine));
    this.router.navigate(['/maquinas/tipos-paradas/']);
  }


  border1Color = (color:any) => `130px solid ${color}`;
  border2Color = (color:any) => `1px solid ${color}`;



}
