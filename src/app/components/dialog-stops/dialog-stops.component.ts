import { Component, inject, ChangeDetectionStrategy, Inject, EventEmitter, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-stops',
  templateUrl: './dialog-stops.component.html',
  styleUrls: ['./dialog-stops.component.scss'],

  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class DialogStopsComponent implements OnInit{
  onSubmitAccept = new EventEmitter();
  readonly dialogRef = inject(MatDialogRef<DialogStopsComponent>);
  isLoading: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any
  ){}

  ngOnInit(): void {

  }

  confirm() {
    this.isLoading = true;
    this.onSubmitAccept.emit(true);

  }
  close(){
    this.dialogRef.close(false);
  }

}
