import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogStopsComponent } from './dialog-stops.component';

describe('DialogStopsComponent', () => {
  let component: DialogStopsComponent;
  let fixture: ComponentFixture<DialogStopsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogStopsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogStopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
