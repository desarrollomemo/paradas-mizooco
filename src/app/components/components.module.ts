import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardMachineComponent } from './card-machine/card-machine.component';
import { ChronometerComponent } from './chronometer/chronometer.component';
import { SnackBarComponent } from './snack-bar/snack-bar.component';
import { MaterialModule } from '../material.module';
import { DialogStopsComponent } from './dialog-stops/dialog-stops.component';
import { DialogLoginComponent } from './dialog-login/dialog-login.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CardMachineComponent,
    ChronometerComponent,
    SnackBarComponent,
    DialogStopsComponent,
    DialogLoginComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  exports: [
    CardMachineComponent,
    ChronometerComponent,
    SnackBarComponent,
    DialogStopsComponent,
    DialogLoginComponent
  ]
})
export class ComponentsModule { }
