import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-unsubscribe-base',
  template: '<!-- unsubscribe.component.html -->',
})
export class UnsubscribeComponent implements OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();

  ngOnDestroy(): void {
    this.destroySubscriptions();
  }

  destroySubscriptions(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
