import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'maquinas',
    loadChildren: () =>import('./modules/modules.module').then( m => m.ModulesModule )
  },
  //{ path: '**', redirectTo: 'maquinas' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
