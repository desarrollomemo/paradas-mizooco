import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BehaviorService {

  isLoadingSubject = new Subject<any>();
  isLoadingObservable = this.isLoadingSubject.asObservable();

  isUserSubject = new Subject<any>();
  isUserObservable = this.isUserSubject.asObservable();

  constructor() { }

  hiddenLoading(opt: boolean) {
    this.isLoadingSubject.next(opt);
  }

  thereUser(data: any) {
    this.isUserSubject.next(data);
  }


}
