import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ParadasService {
  httpOptions:any = {};
  base:string = environment.urlBase.prod;

  constructor(
    private http: HttpClient,
  ) {
    this.httpOptions = { headers: new HttpHeaders({ 'Content-Type':  'application/json'}) };
  }

  /**Métodos get para obtener información de las maquinas */
  getInfoMachines() {
    return this.http.get<any>(`${this.base}${environment.endpoints.maquinas}`, this.httpOptions);
  }

  getStateRedPLC() {
    return this.http.get<any>(`${this.base}${environment.endpoints.estadoRed}`, this.httpOptions);
  }

  /**Método get para obtener los tipos de paradas */
  getTypeStops(id:string = '') {
    return this.http.get<any>(`${this.base}${environment.endpoints.paradas}${id}`, this.httpOptions);
  }

  /**Método para obtener el historial de paradas */
  getHistorialStops() {
    return this.http.get<any>(`${this.base}${environment.endpoints.historial}`, this.httpOptions);
  }

  /**Método para guardar la causa de la parada */
  postStop(data:any){
    return this.http.post<any>(`${this.base}${environment.endpoints.saveParada}`, data, this.httpOptions);
  }

  /**Método para obtener la hora de inicio por cod de maquina */
  getHourInit(id:string){
    return this.http.get<any>(`${this.base}${environment.endpoints.horaInicio}${id}`, this.httpOptions);
  }


}
