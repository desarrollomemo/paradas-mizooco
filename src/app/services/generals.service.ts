import { Injectable } from '@angular/core';
import { TypeStops } from '../interfaces/type-stops.interface';
import { TitlesTypesStopsMap } from '../models/types-stops.model';

@Injectable({
  providedIn: 'root'
})
export class GeneralsService {
  titlesTypesStops:any = TitlesTypesStopsMap;

  constructor() { }

  /**Método para mapear los tipos de paros */
  filtersTypeStops(data:Array<TypeStops>, cod:string) {
    let newData:Array<any> = [];
    switch (cod) {
      case 'PQ':
        newData = [
          {
            title: this.titlesTypesStops[cod].title1,
            acronym: 'IM',
            data: data.filter( item => item.Codigo_tipo_activo === 'IM')
          },
          {
            title: this.titlesTypesStops[cod].title2,
            acronym: 'EM',
            data: data.filter( item => item.Codigo_tipo_activo === 'EM')
          },
          {
            title: this.titlesTypesStops[cod].title3,
            acronym: 'EF',
            data: data.filter( item => item.Codigo_tipo_activo === 'EF')
          },
          {
            title: this.titlesTypesStops[cod].title4,
            acronym: 'SR',
            data: data.filter( item => item.Codigo_tipo_activo === 'SR')
          },
          {
            title: this.titlesTypesStops[cod].title5,
            acronym: 'PD',
            data: data.filter( item => item.Codigo_tipo_activo === 'PD')
          },
          {
            title: this.titlesTypesStops[cod].title6,
            acronym: 'SG',
            data: data.filter( item => item.Codigo_tipo_activo === 'SG')
          }
        ];
        break;
      case 'D':
        newData = [
          {
            title: this.titlesTypesStops[cod].title1,
            acronym: 'IM',
            data: data.filter( item => item.Codigo_tipo_activo === 'IM')
          },
          {
            title: this.titlesTypesStops[cod].title2,
            acronym: 'EM',
            data: data.filter( item => item.Codigo_tipo_activo === 'EM')
          },
          {
            title: this.titlesTypesStops[cod].title3,
            acronym: 'SL',
            data: data.filter( item => item.Codigo_tipo_activo === 'SL')
          },
          {
            title: this.titlesTypesStops[cod].title4,
            acronym: 'CS',
            data: data.filter( item => item.Codigo_tipo_activo === 'CS')
          },
          {
            title: this.titlesTypesStops[cod].title5,
            acronym: 'PD',
            data: data.filter( item => item.Codigo_tipo_activo === 'PD')
          },
          {
            title: this.titlesTypesStops[cod].title6,
            acronym: 'SG',
            data: data.filter( item => item.Codigo_tipo_activo === 'SG')
          }
        ];
        break;
      case 'LX':
        newData = [
          {
            title: this.titlesTypesStops[cod].title1,
            acronym: 'AC',
            data: data.filter( item => item.Codigo_tipo_activo === 'AC')
          },
          {
            title: this.titlesTypesStops[cod].title2,
            acronym: 'CA',
            data: data.filter( item => item.Codigo_tipo_activo === 'CA')
          },
          {
            title: this.titlesTypesStops[cod].title3,
            acronym: 'SE',
            data: data.filter( item => item.Codigo_tipo_activo === 'SE')
          },
          {
            title: this.titlesTypesStops[cod].title4,
            acronym: 'EN',
            data: data.filter( item => item.Codigo_tipo_activo === 'EN')
          },
          {
            title: this.titlesTypesStops[cod].title5,
            acronym: 'PD',
            data: data.filter( item => item.Codigo_tipo_activo === 'PD')
          },
          {
            title: this.titlesTypesStops[cod].title6,
            acronym: 'SG',
            data: data.filter( item => item.Codigo_tipo_activo === 'SG')
          }
        ];
        break;
    }
    return newData;
  }



}
