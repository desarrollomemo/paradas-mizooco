import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  httpOptions:any = {};
  base:string = environment.urlBase.prod;

  constructor(
    private http: HttpClient,
  ) {
    this.httpOptions = { headers: new HttpHeaders({ 'Content-Type':  'application/json'}) };
  }


  validateLoginService(idUser:any) {
    return this.http.get<any>('../../assets/mockdata/data-user.json', this.httpOptions);
  }



}
