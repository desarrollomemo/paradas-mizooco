import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  base:string = environment.urlBase.socket;
  socket1 = new WebSocket(`${this.base}${environment.endpoints.socket}`);
  socket2 = new WebSocket(`${this.base}${environment.endpoints.socket2}`);

  isSocketSubject = new Subject<any>();
  isSocketObservable = this.isSocketSubject.asObservable();

  isSocketStopsSubject = new Subject<any>();
  isSocketStopsObservable = this.isSocketStopsSubject.asObservable();

  constructor() { }

  onConnect() {
    this.socket1.onopen = (event) =>{
      console.log('Conexión establecida');
    }
  }
  onConnectStops() {
    this.socket2.onopen = (event) =>{
      console.log('Conexión establecida de stops');
    }
  }


  listen() {
    this.socket1.onmessage = (event) => {
      this.listenSocket({opt:true, data: JSON.parse(event.data)})
    };
  }
  listenStops() {
    this.socket2.onmessage = (event) => {
      this.listenStopsSocket({optStops:true, data: JSON.parse(event.data)})
    };
  }


  close() {
    this.socket1.onclose = () => {
      console.log('Conexión cerrada');
    };
  }
  closeStops() {
    this.socket2.onclose = () => {
      console.log('Conexión cerrada Stops');
    };
  }


  error() {
    this.socket1.onerror = (error) => {
      throw error;
    };
  }
  errorStops() {
    this.socket2.onerror = (error) => {
      throw error;
    };
  }

  public listenSocket = (data:any) =>{ this.isSocketSubject.next(data); }
  public listenStopsSocket = (data:any) =>{ this.isSocketStopsSubject.next(data); }


}
