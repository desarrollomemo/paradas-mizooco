

function unicos(arr){
    let result = []
    arr.forEach(e=>{
        if(!results.includes(e)){
            results.push(e)
        }
    })
    return results
}

function extraer_fecha(input) {
    var fecha = new Date(input)
    var dia = fecha.getDate()
    var dia = dia.toString().padStart(2, '0')
    var mes = fecha.getMonth() + 1
    var mes = mes.toString().padStart(2, '0')
    var año = fecha.getFullYear()
    fecha_completa = dia + '-' + mes + '-' + año
    return fecha_completa
}

function extraer_fecha2(input){
    var fecha = input.split("T")[0]
    return fecha
}
function extraer_hora(input) {
    var fecha = new Date(input)
    var hora = fecha.getHours().toString().padStart(2, '0');

    var minutos = fecha.getMinutes().toString().padStart(2, '0');

    var segundos = fecha.getSeconds().toString().padStart(2, '0');

    hora_completa = hora + ':' + minutos + ':' + segundos
    return hora_completa
}
function extraer_hora2(input) {
    var fecha = new Date(input)
    var hora = fecha.getUTCHours().toString().padStart(2, '0');

    var minutos = fecha.getMinutes().toString().padStart(2, '0');

    var segundos = fecha.getSeconds().toString().padStart(2, '0');

    hora_completa = hora + ':' + minutos + ':' + segundos
    return hora_completa
}

function create_id() {
    `Crear un ID aleatorio para el motor SHA`
    var bytes = new Uint8Array(16);
    crypto.getRandomValues(bytes);

    var hexString = Array.from(bytes).map(function (byte) {
        return ('0' + byte.toString(16)).slice(-2).toUpperCase()
    }).join('');
    return '0x' + hexString
}


function create_id2(){

    let numeroAleatorio = '';
    const caracteres = '0123456789'; // Puedes agregar letras o caracteres especiales si lo deseas

    for (let i = 0; i < 7; i++) {
        const indiceAleatorio = Math.floor(Math.random() * caracteres.length);
        numeroAleatorio += caracteres.charAt(indiceAleatorio);
    }

    return numeroAleatorio;


}

// Función para obtener valores únicos de cualquier columna de un array de objetos
function unicos_col(datos, columna) {
    // Utilizamos un Set para almacenar los valores únicos, ya que automáticamente elimina duplicados
    const valoresUnicos = new Set();

    // Iteramos sobre cada objeto en el array de datos
    datos.forEach(objeto => {
      // Verificamos si la columna existe en el objeto actual
      if (objeto.hasOwnProperty(columna)) {
        // Si la columna existe, añadimos su valor al Set
        valoresUnicos.add(objeto[columna]);
      }else{
        alert(`La columna ${columna} no existe en la Data`)
      }
    });

    // Convertimos el Set a un array y lo retornamos, ya que los Sets contienen solo valores únicos
    return Array.from(valoresUnicos);
  }




var lock_icon = /*HTML*/`
<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="var(--bs-danger)" class="bi bi-lock-fill" viewBox="0 0 16 16">
    <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2m3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2"/>
</svg>`
var unlock_icon = /*HTML*/`
<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#adadad" class="bi bi-unlock" viewBox="0 0 16 16">
    <path d="M11 1a2 2 0 0 0-2 2v4a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V9a2 2 0 0 1 2-2h5V3a3 3 0 0 1 6 0v4a.5.5 0 0 1-1 0V3a2 2 0 0 0-2-2M3 8a1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1z"/>
</svg>`


var maximize = /*HTML*/`
<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrows-angle-expand" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M5.828 10.172a.5.5 0 0 0-.707 0l-4.096 4.096V11.5a.5.5 0 0 0-1 0v3.975a.5.5 0 0 0 .5.5H4.5a.5.5 0 0 0 0-1H1.732l4.096-4.096a.5.5 0 0 0 0-.707m4.344-4.344a.5.5 0 0 0 .707 0l4.096-4.096V4.5a.5.5 0 1 0 1 0V.525a.5.5 0 0 0-.5-.5H11.5a.5.5 0 0 0 0 1h2.768l-4.096 4.096a.5.5 0 0 0 0 .707"/>
</svg>
`
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function change_C_O(value){
    usrdata = JSON.parse(localStorage.data)
    usrdata.C_O = value
    localStorage.data = JSON.stringify(usrdata)
    window.location.reload()
}
